export interface ITodo {
    id: string;
    complete: boolean;
    description: string;
}
export declare type todoList = ITodo[];
