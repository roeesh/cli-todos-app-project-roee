import { todoList } from "../types/types.js";
export declare function addTaskHandler(task: string, tasks: todoList): todoList;
export declare function showListHandler(flag: string[], tasks: todoList): todoList | never;
export declare function updateTaskHandler(tasksId: string[], tasks: todoList): todoList;
export declare function updateTaskDescription(args: string[], tasks: todoList): todoList;
export declare function removeTaskHandler(tasksId: string[], tasks: todoList): todoList;
