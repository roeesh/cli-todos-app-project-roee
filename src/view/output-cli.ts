import log from "@ajar/marker";

import { todoList } from "../types/types.js";

const HELP_WINDOW = `
usage: 

These are common commands used in various situations:

    npm run start -- add "text"      
        Add a task to todos list.

        EXAMPLE
            npm run start -- add "hw 1"

    npm run start -- ls [ -c ] | [ -a ]
        Show the todos list.
        -c only the completed todos
        -a only the actived todos
        
        EXAMPLE
            npm run start -- ls -c

    npm run start -- update ID ... 
        Update a task in todos list if completed or not.

        EXAMPLE
            npm run start -- update "7btks2620qo" 

    npm run start -- edit ID new-todo 
        Edit the description of a given task.
    
        EXAMPLE
            npm run start -- edit "7btks2620qo" "new-description-for-todo" 
    
    npm run start -- rm [-c] | ID ...
        Remove one todo or more from the todos list.
        -c remove all completed todos

        EXAMPLE
            npm run start -- rm 7btks2620qo
            npm run start -- rm -c
`;

// display tasks
export function showTasks(tasks: todoList): void {
    console.table(tasks);
}

// display help-window
export function showHelpWindow(): void {
    log.cyan(HELP_WINDOW);
}
