// import fs from 'fs/promises';
import { todoList } from "../types/types.js";
import { genarateID } from "../utils/utils-string.js";

export function addTaskHandler(task: string, tasks: todoList): todoList {
    tasks.push({ id: genarateID(), complete: false, description: `${task}` });
    return tasks;
}

export function showListHandler(
    flag: string[],
    tasks: todoList
): todoList | never {
    // there is flag -c or -a
    if (flag.length === 1) {
        const flagValue = flag[0];
        switch (flagValue) {
            case "-c":
                return tasks.filter((task) => task.complete === true);
            case "-a":
                return tasks.filter((task) => task.complete === false);
            default:
                throw new Error("No such flag.");
        }
    } else if (flag.length === 0) {
        return tasks;
    } else {
        throw new Error("No such flag.");
    }
}

export function updateTaskHandler(
    tasksId: string[],
    tasks: todoList
): todoList {
    return tasks.map((task) => {
        if (tasksId.includes(task.id)) {
            return {
                ...task,
                complete: !task.complete,
            };
        } else {
            return task;
        }
    });
}

export function updateTaskDescription(args: string[], tasks: todoList) : todoList {
    const [taskId,newDescription] = args;
    return tasks.map((task) => {
        if (taskId === task.id) {
            return {...task ,description: newDescription};
        } else {
            return task;
        }
    });
}

export function removeTaskHandler(
    tasksId: string[],
    tasks: todoList
): todoList {
    if (tasksId[0] === "-c") {
        return tasks.filter((task) => task.complete === false);
    } else {
        return tasks.filter((task) => !tasksId.includes(task.id));
    }
}
