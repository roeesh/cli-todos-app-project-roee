import { todoList } from "../types/types.js";
import {
    addTaskHandler,
    showListHandler,
    updateTaskHandler,
    removeTaskHandler,
    updateTaskDescription
} from "./todo-controller.js";
import { showHelpWindow, showTasks } from "../view/output-cli.js";
import { save } from "../database/data-access.js";
import { print } from "../utils/utils-cli.js";

export async function execute(
    path: string,
    args: string[],
    todos: todoList
): Promise<void> {
    const operation = args[0];
    
    switch (operation) {
        case "add":
            // Create a new task
            todos = addTaskHandler(args[args.length - 1], todos);
            await save(path, todos);
            showTasks(todos);
            break;

        case "ls":
            try {
                // Read a list of tasks
                todos = showListHandler(args.slice(1), todos);
                showTasks(todos);
            } catch (error) {
                print(error);
                showHelpWindow();
            }
            break;

        case "update":
            // Update a complete flag for each task / or some
            todos = updateTaskHandler(args.slice(1), todos);
            await save(path, todos);
            showTasks(todos);
            break;

        case "edit":
            // Edit the description of the given task            
            todos = updateTaskDescription(args.slice(1), todos);
            await save(path, todos);
            showTasks(todos);
            break;

        case "rm":
            // Delete a specific task / or some / or completed tasks
            todos = removeTaskHandler(args.slice(1), todos);
            await save(path, todos);
            showTasks(todos);
            break;

        default:
            // SHOW HELP WINDOW
            showHelpWindow();
            break;
    }
}
