import log from "@ajar/marker";

// print to cli
export function print(msg: unknown): void {
    log.red(msg);
}

